#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh
source scripts/utils.sh

## Build config
OPTIONS=( "${@:1:${#}-1}" )
IMAGE_CONFIG_NAME="${*: -1}"

declare -A config

# Parse config file
parse_build_config config "${IMAGE_CONFIG_NAME}"

# Parse commandline arguments
parse_build_options config "${OPTIONS[@]}"

# Parse build-info
declare -A buildInfo
parse_build_info buildInfo "${IMAGE_CONFIG_NAME}"

if [[ ! -v config["ociRegistryUrl"] ]] || [[ ! -v config["stage"] ]]; then
  log_error "Usage: ${0} --registry-url <OCI_REGISTRY_URL> --stage <release|staging> [--runtime-image-name <RUNTIME_IMAGE_NAME>] [--cosign-key <COSIGN_KEY_FILE>] [--cosign-pubkey <COSIGN_PUBKEY_FILE>] <IMAGE_CONFIG_NAME>"
  exit 3
fi

if [[ "${config["stage"]}" != "release" ]] && [[ "${config["stage"]}" != "staging" ]]; then
  log_error "Invalid stage '${config["stage"]}'"
  exit 4
fi

## Push image to container registry
log_begin_section "image_push" "Pushing image to container registry"
case "${config["stage"]}" in
  "release") buildInfo["registryImageUrl"]="${config["ociRegistryUrl"]}/${config["runtimeImageName"]}" ;;
  "staging") buildInfo["registryImageUrl"]="${config["ociRegistryUrl"]}/${config["runtimeImageName"]}/staging" ;;
esac

readarray -d ' ' -t IMAGE_TAGS < <(generate_image_tags "${config["runtimePackageVersion"]}" "${buildInfo["installedRuntimePackageVersion"]}")
log_info "Pushing image tags: ${IMAGE_TAGS[*]}"

DIGEST_FILE="$( mktemp --tmpdir=/tmp "${IMAGE_CONFIG_NAME}_remote_image_digest_XXXXX" )"
for imageTag in "${IMAGE_TAGS[@]}"; do
  declare REGISTRY_IMAGE_TAG_URL
  case "${config["stage"]}" in
    "release") REGISTRY_IMAGE_TAG_URL="${buildInfo["registryImageUrl"]}:${imageTag}" ;;
    "staging") REGISTRY_IMAGE_TAG_URL="${buildInfo["registryImageUrl"]}:${imageTag}-${buildInfo["localImageId"]:0:7}" ;;
  esac

  log_info "Pushing image ${config["runtimeImageName"]} to ${REGISTRY_IMAGE_TAG_URL}"
  buildah push --digestfile "${DIGEST_FILE}" --compression-format gzip --compression-level 9 "${buildInfo["localImageId"]}" "${REGISTRY_IMAGE_TAG_URL}"

  buildInfo["remoteDigest"]="$( <"${DIGEST_FILE}" )"
done
rm "${DIGEST_FILE}"
log_end_section "image_push"

## Determine Cosign key for signing
log_begin_section "image_signing" "Signing container image"
if [[ -v config["cosignKey"] ]]; then
  # Use existing cosign key
  log_info "Using provided cosign key"
  COSIGN_KEY_FILE="${config["cosignKey"]}"

  if [[ ! -v config["cosignPubKey"] ]]; then
    log_error "Cosign public key must be specified for provided cosign key"
    exit 5
  fi
  COSIGN_PUBKEY_FILE="${config["cosignPubKey"]}"
else
  # Generate ephemeral cosign key pair for testing / staging
  log_info "Generating random password for ephemeral cosign key"
  COSIGN_PASSWORD="$( generate_random_string 37 )"
  export COSIGN_PASSWORD

  log_info "Generating ephemeral cosign key"
  cosign generate-key-pair

  # Move cosign public key to build directory for archiving
  COSIGN_PUBKEY_FILE="$( get_build_out_path "${IMAGE_CONFIG_NAME}/cosign.pub" )"
  mv cosign.pub "${COSIGN_PUBKEY_FILE}"
  # Move cosign private key to tmp to prevent accidental archiving
  COSIGN_KEY_FILE="$( mktemp --tmpdir=/tmp "${IMAGE_CONFIG_NAME}_cosign_XXXXX.key" )"
  mv cosign.key "${COSIGN_KEY_FILE}"
fi

## Sign image and SBOMs
log_info "Signing image"
if [[ ! -v COSIGN_PASSWORD ]]; then
  log_error "Missing environment variable COSIGN_PASSWORD"
  exit 5
fi

IMAGE_DIGEST_URL="${buildInfo["registryImageUrl"]}@${buildInfo["remoteDigest"]}"

# Sign image
cosign sign --key "${COSIGN_KEY_FILE}" --yes "${IMAGE_DIGEST_URL}"
cosign verify --key "${COSIGN_PUBKEY_FILE}" "${IMAGE_DIGEST_URL}"

declare -A SBOM_FORMATS=( )
SBOM_FORMATS[spdx]="spdxjson"
SBOM_FORMATS[cyclonedx]="cyclonedx"
SBOM_FORMATS[syft]="https://syft.dev/bom"

BUILD_OUT_PATH="$( get_build_out_path "${IMAGE_CONFIG_NAME}" )"

# Attach signed SBOMs
for SBOM_FORMAT in "${!SBOM_FORMATS[@]}"; do
  log_info "Attaching ${SBOM_FORMAT} SBOM attestation to image"
  cosign attest \
    --replace \
    --key "${COSIGN_KEY_FILE}" \
    --predicate "${BUILD_OUT_PATH}/sbom.${SBOM_FORMAT}.json" \
    --type "${SBOM_FORMATS[${SBOM_FORMAT}]}" \
    --yes \
    "${IMAGE_DIGEST_URL}"

  log_info "Verifying ${SBOM_FORMAT} SBOM attestation"
  cosign verify-attestation --key "${COSIGN_PUBKEY_FILE}" --type "${SBOM_FORMATS[${SBOM_FORMAT}]}" --output "text" "${IMAGE_DIGEST_URL}"
done

store_build_info buildInfo "${IMAGE_CONFIG_NAME}"

log_end_section "image_signing"
