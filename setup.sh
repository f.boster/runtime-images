#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh

DNF_CLI_ARGS=(
  --assumeyes
  --repo updates
  --repo fedora
  --setopt=install_weak_deps=false
  --setopt=cachedir="${DNF_CACHE_PATH:-cache/dnf}"
  --nodocs
)

log_begin_section "build_system_init" "Initialize build system"
log_info "Upgrade build system"
dnf upgrade "${DNF_CLI_ARGS[@]}"

log_info "Install build dependencies"
dnf install "${DNF_CLI_ARGS[@]}" dnf-plugins-core buildah podman fuse-overlayfs

# Downgrading crun to version 1.7 because of issue #24
log_info "Downgrade crun"
OS_RELEASE_VERSION="$(source /etc/os-release; echo "${VERSION_ID}")"
CRUN_VERSION="1.7.2"
CRUN_BUILD="4"
dnf downgrade "${DNF_CLI_ARGS[@]}" \
  "https://kojipkgs.fedoraproject.org/packages/crun/${CRUN_VERSION}/${CRUN_BUILD}.fc${OS_RELEASE_VERSION}/$(uname -m)/crun-${CRUN_VERSION}-${CRUN_BUILD}.fc${OS_RELEASE_VERSION}.$(uname -m).rpm"

log_info "Configure fuse-overlayfs for buildah"
mkdir -vp /var/lib/shared/overlay-images /var/lib/shared/overlay-layers
touch /var/lib/shared/overlay-images/images.lock
touch /var/lib/shared/overlay-layers/layers.lock

# Set up environment variables to note that this is not starting with usernamespace and default to isolate the filesystem with chroot.
cp -v contrib/storage.conf /etc/containers/storage.conf
export _BUILDAH_STARTED_IN_USERNS=""
export BUILDAH_ISOLATION=chroot

log_info "Configure subuid and subgid for podman"
echo "containers:2147483647:2147483648" > /etc/subuid
echo "containers:2147483647:2147483648" > /etc/subgid

log_info "Installing syft"
SYFT_VERSION="0.79.0"
curl -sSfL https://raw.githubusercontent.com/anchore/syft/main/install.sh | /usr/bin/env bash -s -- -b /usr/local/bin "v${SYFT_VERSION}"

log_info "Installing cosign"
COSIGN_VERSION="2.2.4"
rpm --install --verbose --hash --excludedocs "https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign-${COSIGN_VERSION}-1.$(uname -m).rpm"

# Required by cosign to utilize buildah / podman credentials
if [ ! -e ~/.docker/config.json ]; then
  mkdir ~/.docker
  ln -s /run/containers/0/auth.json ~/.docker/config.json
fi

log_end_section "build_system_init"
