#!/usr/bin/env bash
set -euf -o pipefail

# Determine the path of this script
if [[ $BASH_SOURCE = */* ]]; then
  SCRIPT_DIR=${BASH_SOURCE%/*}
else
  SCRIPT_DIR=.
fi

source "${SCRIPT_DIR}/../../scripts/log.sh"

## Configuration
if [ ${#} -ne 2 ]; then
  log_error "Usage: ${0} <GITLAB_PROJECT_ID> <GITLAB_REGISTRY_REPO_ID>"
  exit 2
fi

readonly GITLAB_PROJECT_ID="${1}"
readonly GITLAB_REGISTRY_REPO_ID="${2}"
readonly REGISTRY_REPO_API_PATH="projects/${GITLAB_PROJECT_ID}/registry/repositories/${GITLAB_REGISTRY_REPO_ID}"

## Container image registry cleanup
log_begin_section "attachment_cleanup" "Clean up dangling container image attachments"
# Acquire all available image tags
log_info "Fetching image tags from ${REGISTRY_REPO_API_PATH}"
readarray -t IMAGE_TAGS < <(glab api --paginate "${REGISTRY_REPO_API_PATH}/tags" | jq --raw-output ".[].name")

IMAGE_TAGS_REGULAR=()
IMAGE_TAGS_ATTACHMENTS=()
for IMAGE_TAG in "${IMAGE_TAGS[@]}"; do
  # Check if image tag is an attachment
  if [[ "${IMAGE_TAG}" =~ ^([[:alnum:]]+)-([[:xdigit:]]+)\.(sig|att)$ ]]; then
    IMAGE_TAGS_ATTACHMENTS+=( "${IMAGE_TAG}" )
  else
    IMAGE_TAGS_REGULAR+=( "${IMAGE_TAG}" )
  fi
done

log_info "Resolving digests for regular image tags: ${IMAGE_TAGS_REGULAR[*]}"
declare -A IMAGE_TAGS_REGULAR_DIGESTS
for IMAGE_TAG in "${IMAGE_TAGS_REGULAR[@]}"; do
  IMAGE_TAG_DIGEST="$(glab api "${REGISTRY_REPO_API_PATH}/tags/${IMAGE_TAG}" | jq --raw-output ".digest")"
  IMAGE_TAGS_REGULAR_DIGESTS["${IMAGE_TAG_DIGEST}"]="${IMAGE_TAG}"

  log_info "Resolved image tag ${IMAGE_TAG} to digest ${IMAGE_TAG_DIGEST}"
done

log_info "Determine attachments to be deleted"
for IMAGE_TAG in "${IMAGE_TAGS_ATTACHMENTS[@]}"; do
  if [[ "${IMAGE_TAG}" =~ ^([[:alnum:]]+)-([[:xdigit:]]+)\.(sig|att)$ ]]; then
    TAG_DIGEST_ALGO="${BASH_REMATCH[1]}"
    TAG_DIGEST="${BASH_REMATCH[2]}"
  else
    log_error "Cannot parse related image digest from attachment name ${IMAGE_TAG}"
    continue
  fi

  # Delete attachment if related image does not exist
  log_info "Checking if related image with digest ${TAG_DIGEST_ALGO}:${TAG_DIGEST} for attachment ${IMAGE_TAG} exists"
  if [[ ! -v IMAGE_TAGS_REGULAR_DIGESTS["${TAG_DIGEST_ALGO}:${TAG_DIGEST}"] ]]; then
    log_info "Deleting attachment ${IMAGE_TAG} because related image does not exist"
    glab api -X DELETE "${REGISTRY_REPO_API_PATH}/tags/${IMAGE_TAG}"
  else
    log_info "Keeping attachment ${IMAGE_TAG} because related image exists"
  fi
done
log_end_section "attachment_cleanup"
