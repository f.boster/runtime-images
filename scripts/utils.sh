#!/bin/bash
set -euf -o pipefail

export BUILD_OUT_BASE_PATH="build"
export IMAGE_CONFIGS_BASE_PATH="image-configs"
export BUILD_CONFIG_FILE_NAME="build.conf"

declare -A BUILD_CONFIG_OPTIONS=(
  ["--runtime-package"]="runtimePackage"
  ["--runtime-package-version"]="runtimePackageVersion"
  ["--runtime-image-name"]="runtimeImageName"
  ["--base-image"]="baseImage"
  ["--runtime-entrypoint"]="runtimeEntrypoint"
  ["--runtime-user"]="runtimeUser"
  ["--runtime-ports"]="runtimePorts"
  ["--undesired-packages"]="undesiredPackages"
  ["--supplemental-packages"]="supplementalPackages"
  ["--distro"]="distro"
  ["--registry-url"]="ociRegistryUrl"
  ["--cosign-key"]="cosignKey"
  ["--cosign-pubkey"]="cosignPubKey"
  ["--stage"]="stage"
  ["--module-stream"]="moduleStream"
)

function join() {
  local IFS="${1}"
  shift
  echo -n "${*}"
}

function split() {
  readarray -d "${2}" -t "${1}" < <( printf '%s' "${3}" )
}

function get_image_config_path() {
  local imageConfigName="${1}"

  local imageConfigPath
  imageConfigPath="$( realpath -m "${IMAGE_CONFIGS_BASE_PATH}/${imageConfigName}" )"
  if [ ! -d "${imageConfigPath}" ]; then
    log_error "Image config '${imageConfigName}' does not exist"
    return 1
  fi;

  echo -n "${imageConfigPath}"
}

function generate_image_tags() {
  local baseTag="${1}"
  local fullTag="${2}"

  readarray -d '.' -t baseTagComponents < <(printf '%s' "${baseTag}")
  readarray -d '.' -t fullTagComponents < <(printf '%s' "${fullTag}")

  local -i baseTagSize=${#baseTagComponents[@]}
  local -i fullTagSize=${#fullTagComponents[@]}

  if [ ${baseTagSize} -gt ${fullTagSize} ]; then
    log_error "Expected full tag '${fullTag}' to be more specific than base tag '${baseTag}'"
    return 1
  fi

  declare -a tags=( "$( join "." "${baseTagComponents[@]}" )" )
  local -i tagStartIndex=$((baseTagSize))
  local -i maxTagLength=$((fullTagSize - baseTagSize))
  for ((tagLength=1; tagLength <= maxTagLength; tagLength++)); do
    local -a tagComponents=( "${baseTagComponents[@]}" "${fullTagComponents[@]:${tagStartIndex}:${tagLength}}" )

    tags+=( "$( join "." "${tagComponents[@]}" )" )
  done

  echo -n "${tags[@]}"
}

function get_build_out_path() {
  local imageConfigName="${1}"

  echo -n "${BUILD_OUT_BASE_PATH}/${imageConfigName}"
}

function parse_build_config() {
  # shellcheck disable=SC2034
  # returned via reference
  local -n __config="${1}"
  local imageConfigName="${2}"

  local imageConfigPath="$( get_image_config_path "${imageConfigName}" )"
  local buildConfigPath="${imageConfigPath}/${BUILD_CONFIG_FILE_NAME}"

  if [ ! -f "${buildConfigPath}" ]; then
    log_error "No build config found for '${imageConfigName}' in ${IMAGE_CONFIGS_BASE_PATH}/${IMAGE_CONFIG_NAME}"
    return 4
  fi

  parse_config __config "${buildConfigPath}"
}

function parse_config() {
  local -n _config="${1}"
  local configPath="${2}"

  while IFS=$'\r=' read -r configKey configValue; do
    if [[ -z "${configKey}" ]]; then
      continue
    fi

    # shellcheck disable=SC2034
    # returned via reference
    _config[${configKey}]="${configValue}"
  done < "${configPath}"
}

function parse_build_options() {
  local -n _config="${1}"
  local -a _options=( "${@:2}" )

  for (( i = 0; i < ${#_options[@]}; )); do
    local option="${_options[$(( i++ ))]}"

    if [[ ! -v BUILD_CONFIG_OPTIONS[${option}] ]]; then
      log_error "Unknown option ${option}"
      return 1
    fi

    configKeyName="${BUILD_CONFIG_OPTIONS[${option}]}"

    # shellcheck disable=SC2034
    # returned via reference
    _config[${configKeyName}]="${_options[$(( i++ ))]}"
  done
}

function store_build_info() {
  local -n _buildInfo="${1}"
  local imageConfigName="${2}"

  {
    for configKey in "${!_buildInfo[@]}"; do
      echo "${configKey}=${_buildInfo[${configKey}]}"
    done
  } > "$(get_build_out_path "${imageConfigName}")/build-info.conf"
}

function parse_build_info() {
  local -n _buildInfo="${1}"
  local imageConfigName="${2}"

  parse_config _buildInfo "$(get_build_out_path "${imageConfigName}")/build-info.conf"
}

function generate_random_string() {
  local -i randomStringSize="${1}"
  local -r availableCharacters='ABCDEFGHIJKLMNOPQJRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~'

  local randomString=""
  for (( i=0; i<randomStringSize; i++ )); do
      randomString+="${availableCharacters:RANDOM%${#availableCharacters}:1}"
  done

  echo -n "${randomString}"
}
