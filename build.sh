#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh
source scripts/utils.sh

readonly DISTRO_CONFIGS_DIR="distro-configs"
readonly FILESYSTEM_CONTENT_DIR="content"
readonly HOOKS_DIR="hooks"
readonly PODMAN_URI="unix:///tmp/podman.sock"
readonly WORKING_DIRECTORY="/app"

readonly DEFAULT_DISTRO="ubi"
readonly DEFAULT_BASE_IMAGE="scratch"
readonly DEFAULT_RUNTIME_ENTRYPOINT=""
readonly DEFAULT_RUNTIME_USER_NAME="app"
readonly DEFAULT_RUNTIME_USER_ID="1001"
RUNTIME_PORTS=( )
UNDESIRED_PACKAGES=( )
SUPPLEMENTAL_PACKAGES=( )

## Build config
OPTIONS=( "${@:1:${#}-1}" )
IMAGE_CONFIG_NAME="${*: -1}"

IMAGE_CONFIG_PATH="$( get_image_config_path "${IMAGE_CONFIG_NAME}" )"
declare -A config

# Parse config file
parse_build_config config "${IMAGE_CONFIG_NAME}"

# Parse commandline arguments
parse_build_options config "${OPTIONS[@]}"

# Set default config
if [[ ! -v config["baseImage"] ]]; then
  config["baseImage"]="${DEFAULT_BASE_IMAGE}"
fi
if [[ ! -v config["runtimeUserName"] ]]; then
  config["runtimeUserName"]="${DEFAULT_RUNTIME_USER_NAME}"
fi
if [[ ! -v config["runtimeUserId"] ]]; then
  config["runtimeUserId"]="${DEFAULT_RUNTIME_USER_ID}"
fi
if [[ ! -v config["runtimeEntrypoint"] ]]; then
  config["runtimeEntrypoint"]="${DEFAULT_RUNTIME_ENTRYPOINT}"
fi
if [[ ! -v config["distro"] ]]; then
  config["distro"]="${DEFAULT_DISTRO}"
fi

if [[ ! -v config["runtimePackage"] ]] || [[ ! -v config["runtimePackageVersion"] ]] || [[ ! -v config["runtimeImageName"] ]]; then
  log_error \
  "Usage: ${0}
  --runtime-package <RUNTIME_PACKAGE> --runtime-package-version <RUNTIME_PACKAGE_VERSION> --runtime-image-name <RUNTIME_IMAGE_NAME>
  [--base-image <BASE_IMAGE>] [--runtime-entrypoint <RUNTIME_ENTRYPOINT>] [--runtime-user <RUNTIME_USER>] [--runtime-port <RUNTIME_PORT>...]
  [--undesired-package <UNDESIRED_PACKAGE>...] [--supplemental-package <SUPPLEMENTAL_PACKAGE>...] [--distro (ubi|fedora)]
  <IMAGE_CONFIG_NAME>"
  exit 3
fi

# Get distro release version
IFS=$'\r' read -r config["distroVersion"] < "${DISTRO_CONFIGS_DIR}/${config["distro"]}/RELEASE"

# Print effective config
log_info "
Base Image: ${config["baseImage"]}
Distro: ${config["distro"]}:${config["distroVersion"]}
Runtime Package: ${config["runtimePackage"]}:${config["runtimePackageVersion"]}
Target Image Name: ${config["runtimeImageName"]}
Entrypoint: ${config["runtimeEntrypoint"]:-}
User: ${config["runtimeUserName"]} (UID/GID: ${config["runtimeUserId"]})
Exposed Ports: ${config["runtimePorts"]:-}
Undesired Packages: ${config["undesiredPackages"]:-}
"

## Initialize container
log_begin_section "container_init" "Initialize container"
# Create new container from base image
CONTAINER_ID="$( buildah from "${config["baseImage"]}" )"
log_info "Created container ${CONTAINER_ID} from base image ${config["baseImage"]}"

# Mount new container
CONTAINER_MOUNT="$( buildah mount "${CONTAINER_ID}" )"
log_info "Mounted container ${CONTAINER_ID} at ${CONTAINER_MOUNT}"

# Configure DNF arguments
declare -a DNF_CLI_ARGS=(
  --installroot "${CONTAINER_MOUNT}"
  --disableplugin=subscription-manager
  --releasever="${config["distroVersion"]}"
  --setopt=install_weak_deps=false
  --setopt=reposdir="${DISTRO_CONFIGS_DIR}/${config["distro"]}/"
  --setopt=cachedir="${DNF_CACHE_PATH:-cache/dnf}"
  --setopt=tsflags=nodocs
  --nodocs
  --assumeyes
)

# Configure RPM arguments
declare -a RPM_CLI_ARGS=(
  --root "${CONTAINER_MOUNT}"
)

# Create build output directory
BUILD_OUT_PATH="$( get_build_out_path "${IMAGE_CONFIG_NAME}" )"
if [ -e "${BUILD_OUT_PATH}" ]; then
  find "${BUILD_OUT_PATH}" -mindepth 1 -delete
else
  mkdir -p "${BUILD_OUT_PATH}"
fi
log_end_section "container_init"

## Install packages
log_begin_section "package_installation" "Install runtime package"
# Required to make the generation of cacerts reproducible (https://github.com/p11-glue/p11-kit/issues/379)
export SOURCE_DATE_EPOCH="1"

# Import RPM GPG key for distro
log_info "Importing RPM GPG key ${DISTRO_CONFIGS_DIR}/${config["distro"]}/RPM-GPG-KEY for distro ${config["distro"]}:${config["distroVersion"]}"
rpm --import -v "${RPM_CLI_ARGS[@]}" "${DISTRO_CONFIGS_DIR}/${config["distro"]}/RPM-GPG-KEY"

# Update all packages
log_info "Upgrading packages"
dnf upgrade "${DNF_CLI_ARGS[@]}"

if [ -n "${config["moduleStream"]:-}" ]; then
	log_info "Enabling module stream ${config["moduleStream"]}"
	dnf module enable "${DNF_CLI_ARGS[@]}" "${config["moduleStream"]}"
fi

log_info "Dumping DNF dependency tree"
dnf repoquery "${DNF_CLI_ARGS[@]}" \
  --tree --queryformat "%{NAME}" --requires --resolve \
  "${config["runtimePackage"]}-${config["runtimePackageVersion"]}*" \
  > "${BUILD_OUT_PATH}/dnf-dependency-tree.txt"

# Install runtime package
log_info "Installing runtime package ${config["runtimePackage"]}:${config["runtimePackageVersion"]}"
declare -a REQUIRED_SUPPLEMENTAL_PACKAGES=(
  # Required for container security scanners to identify distro-specific vulnerabilities
  "system-release"
)

declare -a SUPPLEMENTAL_PACKAGES
if [ -v config["supplementalPackages"] ]; then
  split SUPPLEMENTAL_PACKAGES " " "${config["supplementalPackages"]}"
fi
echo "Supplemental packages: ${SUPPLEMENTAL_PACKAGES[*]}"

dnf install "${DNF_CLI_ARGS[@]}" "${config["runtimePackage"]}-${config["runtimePackageVersion"]}*" "${REQUIRED_SUPPLEMENTAL_PACKAGES[@]}" "${SUPPLEMENTAL_PACKAGES[@]}"
INSTALLED_RUNTIME_PACKAGE_VERSION="$( dnf repoquery "${DNF_CLI_ARGS[@]}" -q --installed --queryformat "%{version}" "${config["runtimePackage"]}-${config["runtimePackageVersion"]}*" )"
log_info "Installed version for runtime package ${config["runtimePackage"]}: ${INSTALLED_RUNTIME_PACKAGE_VERSION}"

# Remove undesired packages
declare -a UNDESIRED_PACKAGES
split UNDESIRED_PACKAGES " " "${config["undesiredPackages"]}"

if [ ${#UNDESIRED_PACKAGES[@]} -gt 0 ]; then
  log_info "Removing undesired packages: ${UNDESIRED_PACKAGES[*]}"
  rpm --erase -v "${RPM_CLI_ARGS[@]}" --nodeps "${UNDESIRED_PACKAGES[@]}"
  dnf autoremove "${DNF_CLI_ARGS[@]}"
fi
log_info "Removing bash and coreutils"
rpm --erase -v "${RPM_CLI_ARGS[@]}" --nodeps bash coreutils
dnf autoremove "${DNF_CLI_ARGS[@]}"
log_info "Removing setup"
rpm --erase -v "${RPM_CLI_ARGS[@]}" --nodeps setup
dnf autoremove "${DNF_CLI_ARGS[@]}"
log_info "Removing leftover RPM saves"
find "${CONTAINER_MOUNT}/" -name "*.rpmsave" -type f -ls -delete

# Dump current RPM / DNF state
log_info "Installed package sizes:"
rpm --query "${RPM_CLI_ARGS[@]}" --all --queryformat '%11{SIZE} %{NAME}\n' | sort -k1n
log_info "Dumping DNF / RPM state"
dnf debug-dump "${DNF_CLI_ARGS[@]}" --norepos "${BUILD_OUT_PATH}/dnf-state.txt"
log_end_section "package_installation"

## User management
log_begin_section "user_management" "Add user management"
log_info "Adding user / group root with UID / GID 0"
groupadd --root "${CONTAINER_MOUNT}" --gid 0 --system root
useradd --root "${CONTAINER_MOUNT}" --no-log-init --no-create-home --uid 0 --gid 0 --system --home-dir /root root
log_info "Adding user / group ${config["runtimeUserName"]} with UID / GID ${config["runtimeUserId"]}"
groupadd --root "${CONTAINER_MOUNT}" --gid "${config["runtimeUserId"]}" --system "${config["runtimeUserName"]}"
useradd --root "${CONTAINER_MOUNT}" --no-log-init --no-create-home --uid "${config["runtimeUserId"]}" --gid "${config["runtimeUserId"]}" --system --home-dir /app "${config["runtimeUserName"]}"
log_end_section "user_management"

## Cleanup container to make the build reproducible
log_begin_section "container_cleanup" "Cleanup container"
log_info "Cleaning DNF caches"
dnf clean all expire-cache -v "${DNF_CLI_ARGS[@]}"

log_info "Removing caches, logs and further undesired files for reproducibility"
find "${CONTAINER_MOUNT}/var/lib/dnf/" -mindepth 1 -ls -delete
find "${CONTAINER_MOUNT}/var/cache/" -mindepth 1 -ls -delete
find "${CONTAINER_MOUNT}/var/log/" -mindepth 1 -type f -ls -delete
find "${CONTAINER_MOUNT}/tmp/" -mindepth 1 -ls -delete

# https://www.freedesktop.org/software/systemd/man/machine-id.html
if [ -f "${CONTAINER_MOUNT}/etc/machine-id" ]; then
  log_info "Clearing machine-id"
  truncate --size 0 "${CONTAINER_MOUNT}/etc/machine-id"
fi
log_end_section "container_cleanup"

## Rebuild / compact RPM database
log_begin_section "rpmdb_rebuild" "Rebuilding / compacting RPM database"
rpm --rebuilddb -v "${RPM_CLI_ARGS[@]}"
# This command is required for the rpmdb to be reproducible
rpm --query "${RPM_CLI_ARGS[@]}" --all --queryformat '%{NAME} %{INSTALLTIME}\n' > /dev/null

# Create a link from the current RPM database location to the legacy RPM database path "/var/lib/rpm" for compatibility with container scanners
LEGACY_RPMDB_PATH="/var/lib/rpm"
RPMDB_PATH="$( rpm --eval="%{_dbpath}" "${RPM_CLI_ARGS[@]}" )"
if [[ "${RPMDB_PATH}" != "${LEGACY_RPMDB_PATH}" ]] && [[ ! -e "${CONTAINER_MOUNT}${LEGACY_RPMDB_PATH}" ]]; then
  log_info "Creating symlink from RPM database location ${RPMDB_PATH} to legacy RPM database location ${LEGACY_RPMDB_PATH}"
  ln -vs "$( realpath -m --relative-to="$( dirname "${LEGACY_RPMDB_PATH}" )" "${RPMDB_PATH}" )" "${CONTAINER_MOUNT}${LEGACY_RPMDB_PATH}"
fi
log_end_section "rpmdb_rebuild"

## Execute post-install hook
if [[ -e "${IMAGE_CONFIG_PATH}/${HOOKS_DIR}/post-install.sh" ]]; then
  log_begin_section "post_install_hook" "Running post install hook"

  CONTAINER_ID="${CONTAINER_ID}" CONTAINER_MOUNT="${CONTAINER_MOUNT}" RUNTIME_USER_ID="${config["runtimeUserId"]}" "${IMAGE_CONFIG_PATH}/${HOOKS_DIR}/post-install.sh"
  log_end_section "post_install_hook"
fi

## Overwrite content and configuration files
if [[ -d "${IMAGE_CONFIG_PATH}/${FILESYSTEM_CONTENT_DIR}" ]]; then
  log_begin_section "image_content" "Copying / overwriting image content"
  cp -v --recursive "${IMAGE_CONFIG_PATH}/${FILESYSTEM_CONTENT_DIR}/." "${CONTAINER_MOUNT}"
  log_end_section "image_content"
fi

## Security: Defang setuid / setgid binaries
log_begin_section "security_defang" "Defanging setuid / setgid binaries"
find "${CONTAINER_MOUNT}" -type f -perm /6000 -ls -exec chmod -v a-s "{}" \;
log_end_section "security_defang"

## Configure image
log_begin_section "image_configuration" "Configuring image"

# Set working directory
log_info "Setting image working director: ${WORKING_DIRECTORY}"
mkdir -pv "${CONTAINER_MOUNT}${WORKING_DIRECTORY}"
buildah config --workingdir "${WORKING_DIRECTORY}" "${CONTAINER_ID}"

# Set entrypoint
if [ -n "${config["runtimeEntrypoint"]}" ]; then
  log_info "Setting image command: ${config["runtimeEntrypoint"]}"
  buildah config --cmd "[ \"${config["runtimeEntrypoint"]}\" ]" "${CONTAINER_ID}"
fi

# Set user
log_info "Setting image user: ${config["runtimeUserName"]}"
buildah config --user "${config["runtimeUserName"]}" "${CONTAINER_ID}"

# Set exposed ports
if [[ -v config["runtimePorts"] ]]; then
  declare -a RUNTIME_PORTS
  split RUNTIME_PORTS " " "${config["runtimePorts"]}"

  for runtime_port in "${RUNTIME_PORTS[@]}"; do
    log_info "Adding exposed port: ${runtime_port}"
    buildah config --port "${runtime_port}" "${CONTAINER_ID}"
  done
fi

# Add volumes
if [[ -v config["runtimeVolumes"] ]]; then
  declare -a VOLUMES
  split VOLUMES " " "${config["runtimeVolumes"]}"

  for volume in "${VOLUMES[@]}"; do
    log_info "Adding volume: ${volume}"
    buildah config --volume "${volume}" "${CONTAINER_ID}"
  done
fi
log_end_section "image_configuration"

## Commit container to image
log_begin_section "image_commit" "Commiting container to image"
# Unmount new container
buildah umount "${CONTAINER_ID}"
# Commit new container to new image
log_info "Committing container to image ${config["runtimeImageName"]}"
IMAGE_ID="$( buildah commit --timestamp 0 "${CONTAINER_ID}" "${config["runtimeImageName"]}" )"
buildah images --digests "${IMAGE_ID}"

# Determine appropriate tags according to runtime package version
readarray -d ' ' -t IMAGE_TAGS < <(generate_image_tags "${config["runtimePackageVersion"]}" "${INSTALLED_RUNTIME_PACKAGE_VERSION}")
log_info "Image tags: ${IMAGE_TAGS[*]}"
log_end_section "image_commit"

## Build SBOM
log_begin_section "sbom_build" "Building SBOM"
log_info "Starting Podman daemon"
podman system service --time=0 "${PODMAN_URI}" &
PODMAN_PID="${!}"

log_info "Building SBOM"
CONTAINER_HOST="${PODMAN_URI}" syft packages "podman:sha256:${IMAGE_ID}" \
  -o table \
  -o "json=${BUILD_OUT_PATH}/sbom.syft.json" \
  -o "cyclonedx-json=${BUILD_OUT_PATH}/sbom.cyclonedx.json" \
  -o "spdx-json=${BUILD_OUT_PATH}/sbom.spdx.json" \
  -o "github=${BUILD_OUT_PATH}/sbom.github.json"

kill "${PODMAN_PID}"
log_end_section "sbom_build"

## Write build-info
declare -A BUILD_INFO=(
  ["imageConfig"]="${IMAGE_CONFIG_NAME}"
  ["localImageId"]="${IMAGE_ID}"
  ["installedRuntimePackageVersion"]="${INSTALLED_RUNTIME_PACKAGE_VERSION}"
)

store_build_info BUILD_INFO "${IMAGE_CONFIG_NAME}"
