#!/usr/bin/env bash
set -euf -o pipefail

source scripts/log.sh

readonly CONTAINER_IP="${1}"
readonly CONTAINER_ID="${2}"

log_info "Check status"
podman exec "${CONTAINER_ID}" /usr/sbin/unbound-control status

log_info "Check config sanity"
podman exec "${CONTAINER_ID}" /usr/sbin/unbound-checkconf
