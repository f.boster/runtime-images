#!/usr/bin/env bash
set -euf -o pipefail

# Create custom configuration directory
mkdir -v "${CONTAINER_MOUNT}/etc/unbound/custom-conf.d"
